package fr.ib.mickael.zoo;

import java.io.IOException;
import java.io.Writer;
import java.time.DayOfWeek;
import java.time.LocalDateTime;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mic
 *
 */
public class PremiereVisiteServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	// eviter : private int v = 3;
	
	@Override
	public void init(ServletConfig config) throws ServletException {		
		super.init(config);
		System.out.println("**** PremiereVisiteServlet : init()");
	}

	@Override
	public void destroy() {
		System.out.println("**** PremiereVisiteServlet : destroy()");
		super.destroy();
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setStatus(200);
		resp.setContentType("text/html");
		resp.addHeader("Test-Mode", "true");

		//envoyer un cookie :
		Cookie c = new Cookie("dejavenu", "oui");
		c.setMaxAge(60); // en production, mettre 60*60*24*365
		//c.setDomain("ib-formation.fr");
		resp.addCookie(c);
				
		Writer sortie = resp.getWriter();
		String titre = getInitParameter("titre_du_site");
		sortie.write("<!DOCTYPE html><html><head><title>"+titre+"</title></head><body>");
		sortie.write("<h1>Première visite ?</h1>");
		
		// lire les cookies :
		boolean dejavenu = false;
		Cookie[] cookies = req.getCookies();
		if(cookies!=null) {
			for(Cookie cookie:cookies) {
				if(cookie.getName().equals("dejavenu"))
					dejavenu = true;
			}
		}
		if(!dejavenu) {
			sortie.write("<h2>Bienvenue</h2>");	
		} else {
			sortie.write("<h2>Re-bienvenue</h2>");
		}
		
		String userAgent = req.getHeader("User-Agent");
		System.out.println("User-Agent : "+userAgent);
		if(userAgent.contains("Android") || userAgent.contains("iPhone")) {
			sortie.write("<p>N'oubliez pas de télécharger notre App spéciale du zoo</p>");
		}
		
		sortie.write("</body></html>");
		sortie.close();
	}
	
	
}
