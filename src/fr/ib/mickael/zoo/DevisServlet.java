/**
 * 
 */
package fr.ib.mickael.zoo;

import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mic
 */
public class DevisServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int maxVisiteurs = 8;
		String[] offres = {	"Visite simple","Visite commentée",	"Visite de nuit","Une semaine enfermés"};
		req.setAttribute("maxVisiteurs", maxVisiteurs);
		req.setAttribute("offres", offres);
		req.getRequestDispatcher("/WEB-INF/vues/devis.jsp").forward(req, resp);
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		String nom = req.getParameter("nom");
		String email = req.getParameter("email");
		int visiteurs = Integer.parseInt(req.getParameter("visiteurs")) ;
		String date = req.getParameter("date");
		String offre = req.getParameter("offre");
		System.out.println("Reçu : "+nom+";"+email+";"+visiteurs+";"+date+";"+offre);
		
		if(nom==null || nom.trim().equals("")) {
			req.setAttribute("message", "Nom incorrect");
			req.setAttribute("nom", nom);
			req.setAttribute("email", email);
			req.setAttribute("visiteurs", visiteurs);
			doGet(req, resp);
		} else {		
			FileWriter fw = new FileWriter("c:\\users\\mic\\documents\\devis.txt", true);
			fw.write(nom+";"+email+";"+visiteurs+";"+date+";"+offre+"\n");
			fw.close();
			
			req.setAttribute("prix", visiteurs * 260);
			req.getRequestDispatcher("/WEB-INF/vues/devis-succes.jsp").forward(req, resp);
		}
	}
}
