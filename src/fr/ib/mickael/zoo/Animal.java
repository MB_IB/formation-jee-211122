/**
 * 
 */
package fr.ib.mickael.zoo;

/**
 * @author mic
 *
 */
public class Animal {
	private String nom, espece;
	private int age;
	
	public Animal(String nom, String espece, int age) {
		super();
		this.nom = nom;
		this.espece = espece;
		this.age = age;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEspece() {
		return espece;
	}
	public void setEspece(String espece) {
		this.espece = espece;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
}
