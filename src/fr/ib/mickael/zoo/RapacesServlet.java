/**
 * 
 */
package fr.ib.mickael.zoo;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mic
 *
 */
public class RapacesServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] noms = {"Ann l'aigle", "Bob le vautour", "Carl le gypaète barbu"};
		resp.setContentType("text/html");
		Writer sortie = resp.getWriter();
		sortie.write("<!DOCTYPE html><html><head><title>Zoo</title></head><body>");
		sortie.write("<h1>Rapaces</h1>");
		
		// afficher une liste à puce des rapaces
		sortie.write("<ul>");
		for(int i=0;i<noms.length;i++)
			// http://..../../page?a=1&b=2&c=xyz
			sortie.write("<li><a href='rapaces?numero="+i+"'>"+noms[i]+"</a></li>");
		sortie.write("</ul>");
		
		String numero = req.getParameter("numero");
		if(numero!=null) {
			int numeroEntier = Integer.parseInt(numero);
			if(numeroEntier>=0 && numeroEntier<noms.length) {
				sortie.write("<h2>"+noms[numeroEntier]+"</h2>");
			}
		}
		
		sortie.write("</body></html>");
		sortie.close();
	}
	
}
