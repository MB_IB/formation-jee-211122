/**
 * 
 */
package fr.ib.mickael.zoo;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mic
 *
 */
public class VisiteServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		Writer sortie = resp.getWriter();
		sortie.write("<!DOCTYPE html><html><head><title>Zoo</title></head><body>");
		sortie.write("<h1>Ta visite au zoo</h1>");
		sortie.write("<form action='visite' method='post' enctype='multipart/form-data'>");
		sortie.write("<p><label>Mon nom <input type='text' name='nom' required></label></p>");
		sortie.write("<p><label>Mon age <input name='age' type='number' min='1' max='77'></label></p>");
		sortie.write("<p><label>1ère fois au zoo ? <input name='premiere' type='checkbox'></label></p>");
		sortie.write("<p><label>Je les ai vu : <select name='animaux' multiple>"
				+ "<option>Ann la lionne</option>"
				+ "<option>Bob l'aigle</option>"
				+ "<option>Carl l'hippopotame</option>"				
				+ "</select></label></p>");
		sortie.write("<p><label>Une photo que j'ai pris <input name='photo' type='file'></label></p>");
		sortie.write("<p><input type='submit'></p>");
		sortie.write("</form>");
	
		sortie.write("</body></html>");
		sortie.close();
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		Writer sortie = resp.getWriter();
		sortie.write("<!DOCTYPE html><html><head><title>Zoo</title></head><body>");
		sortie.write("<h2>Visite enregistrée</h2>");
		sortie.write("<p>Mes informations:</p>");
		
		sortie.write("<p>Nom : "+req.getParameter("nom")+"</p>");
		
		int age = Integer.parseInt(req.getParameter("age"));
		sortie.write("<p>Age : "+age+"</p>");
		
		if(req.getParameter("premiere")==null)
			sortie.write("<p>Ce n'est pas ma premiere visite</p>");
		else
			sortie.write("<p>C'est ma premiere visite !</p>");
		
		String[] animaux = req.getParameterValues("animaux");
		sortie.write("<p>J'ai vu :</p><ul>");
		for(String animal:animaux) {
			sortie.write("<li>"+animal+"</li>");
		}
		sortie.write("</ul>");
		
		sortie.write("</body></html>");
		sortie.close();
	}
	
	
}
