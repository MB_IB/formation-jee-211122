package fr.ib.mickael.zoo;

import java.io.IOException;
import java.io.Writer;
import java.time.DayOfWeek;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mic
 *
 */
public class OuvertureServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	private boolean ouvert() {
		LocalDateTime dt = LocalDateTime.now();
		int heure = dt.getHour();
		DayOfWeek jour = dt.getDayOfWeek();
		// Zoo ouvert du mardi au dimanche entre 9h00 et 18h00
		return jour!=DayOfWeek.MONDAY && heure>=9 && heure<18;
	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// type MIME de la réponse :
		resp.setContentType("text/html");
		Writer sortie = resp.getWriter();
		// binaire : resp.getOutputStream()
		sortie.write("<!DOCTYPE html><html><head><title>Zoo</title></head><body>");
		sortie.write("Ouverture du zoo : "); 
		if(ouvert()) {
			sortie.write("<strong>Oui</strong>");
		} else {
			sortie.write("Non");
		}
		sortie.write("</body></html>");
		sortie.close();
	}
	
	
}
