/**
 * 
 */
package fr.ib.mickael.zoo;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mic
 *
 */
public class EnclosServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String[] noms = {"Cage des tigres", "Enclos des singes", "Buvette"};
		int[] aires = {85, 12600, 34}; //m²
		
		resp.setContentType("text/html");
		Writer sortie = resp.getWriter();
		sortie.write("<!DOCTYPE html><html><head><title>Zoo</title>");
		//lien http://localhost:8080/mon_zoo/enclos ->
		//     http://localhost:8080/mon_zoo/site.css
		sortie.write("<link rel='stylesheet' href='site.css'>");
		sortie.write("</head><body>");
		sortie.write("<h1>Les enclos</h1><table>");
		// afficher les enclos
		sortie.write("<thead><tr><th>Nom</th><th>Aire</th></tr></thead>");
		sortie.write("<tbody>");
		for(int i=0; i<noms.length; i++) {
			sortie.write("<tr><td>"+noms[i]+"</td><td>"+aires[i]+"m²</td></tr>");			
		}				
		sortie.write("</tbody>");
		sortie.write("</table>");
		// lien à cliquer vers l'ouverture
		// même projet, même serveur
		sortie.write("<p><a href=\"ouverture\">Ouverture</a></p>");
		// autre projet, même serveur		
		sortie.write("<p><a href=\"/mon_zoo/ouverture\">Ouverture</a></p>");
		// autre serveur		
		sortie.write("<p><a href=\"http://localhost:8080/mon_zoo/ouverture\">Ouverture</a></p>");
		
		sortie.write("</body></html>");
		sortie.close();
	}
	
}
