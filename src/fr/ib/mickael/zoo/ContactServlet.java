/**
 * 
 */
package fr.ib.mickael.zoo;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author mic
 *
 */
public class ContactServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		Writer sortie = resp.getWriter();
		sortie.write("<!DOCTYPE html><html><head><title>Zoo</title></head><body>");
		sortie.write("<h1>Contactez nous</h1>");
		sortie.write("<form>");
		sortie.write("<p><label>Votre nom <input type='text' name='nom' required></label></p>");
		sortie.write("<p><label>Votre téléphone <input type='tel' name='tel'></label></p>");
		sortie.write("<p><label>Votre email <input type='email' name='email'></label></p>");
		sortie.write("<p><label>Message <textarea name='message'></textarea></p>");
		sortie.write("<p><input type='submit'></p>");
		sortie.write("</form>");
		
		// Soit :
		// - rien afficher (arrivée sur le formulaire)
		// - afficher "nom manquant" ou "email manquant" ou...
		// - afficher "message envoyé"
		if(req.getParameter("nom")!=null) {	
			if(req.getParameter("nom").trim().equals("")) {
				sortie.write("<p>Nom manquant</p>");				
			} else if(req.getParameter("tel").trim().equals("") && 
					req.getParameter("email").trim().equals("")) {
				sortie.write("<p>Tel et Email manquants</p>");				
			} else if(req.getParameter("message").trim().equals("")) {
				sortie.write("<p>Message manquant</p>");				
			} else {
				sortie.write("<p>Message envoyé</p>");
			}
		}
		// String x = null;  
		// n'est pas    
		// String x = "";
		
		sortie.write("</body></html>");
		sortie.close();
	}
}
