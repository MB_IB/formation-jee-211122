<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="fr.ib.mickael.zoo.Animal" %>    
<%@ page import="java.util.ArrayList" %>
<%
String description = "Notre enclos du marais montre les espèces les plus " + 
	"emblématiques des deltas d'Europe et d'Afrique.";
pageContext.setAttribute("description", description);

int rayon = 38; // metres
pageContext.setAttribute("rayon", rayon);

Animal mascotte = new Animal("Dan", "Loutre", 7);
pageContext.setAttribute("mascotte", mascotte);

ArrayList<Animal> animaux = new ArrayList<>();
animaux.add(new Animal("Eloise", "Alligator", 13));
animaux.add(new Animal("Frank", "Ragondin", 3));
animaux.add(mascotte);
pageContext.setAttribute("animaux", animaux);
%>
<!DOCTYPE html><html>
<head><meta charset="utf8"><title>Zoo</title></head>
<body>
	<h1>Le marais</h1>
	<p>${description}</p>
	<p>Cet enclos fait environ ${ Math.round(rayon*rayon*Math.PI) } m² !</p>
	<h2>Notre mascotte : ${mascotte.getNom()}</h2>
	<p>Espèce : ${mascotte.espece}</p>
	<p>Age : ${mascotte.age}</p>
	<p>Ami de : ${animaux[0].nom}</p>
</body>
</html>