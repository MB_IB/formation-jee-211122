<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Mon Zoo</title>
</head>
<body>
	<h1>La savane</h1>
	<%! static final String[] ANIMAUX = {"Lions", "Elephants", "Girafes", "Hippopotames" }; %>
	<ul>		
		<% for(String animal:ANIMAUX) { %>
			<li>
				<a href="animal.jsp?animal=<%= animal %>" ><%= animal %></a>
			</li>			
		<% } %>		
	</ul>
	<%-- heures a rester en fonction du nombre d'animaux a voir (1...) et du nombre
	de personne dans le groupe (1..6)
	minutes = nbanimaux * 25 * (nbgroupe+3) --%>
	<table>
		<thead>
			<tr><th>Animaux \ Personnes</th>
			<% for(int p = 1; p<=6 ; p++) { %>
				<th><%= p %></th>
			<% } %>
			</tr>
		</thead>
		<tbody>
		<% for(int a = 1; a<=ANIMAUX.length; a++) { %>		
			<tr>
				<th><%= a %></th>
			<% for(int p = 1; p<=6 ; p++) { %>
				<td>
					<%= (int)Math.round(a*25*(p+3)/60.0) %> heures
				</td>			
			<% } %>
			</tr>		
		<% } %>
		</tbody>
	</table>	

</body>
</html>