<%@ page import="java.time.LocalDateTime"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
<!DOCTYPE html>
<html>
<% String nomPage = "Animal"; %>
<%@ include file="head.inc.jsp" %>
<body>
	<% if(request.getParameter("animal")==null || 
			request.getParameter("animal").trim().equals("")) { 
		// response.setStatus(404); // erreur non trouvé
		// response.sendRedirect("erreur404.jsp"); // redirection temporaire 302
		response.setStatus(301); // redirection permanente 301
		response.setHeader("Location", "erreur404.jsp");
	} else { %>
		<h1><%= request.getParameter("animal") %></h1>
		<p>Visibles en ce moment ? 
		<%-- afficher OUI s'il est entre 7h et 18h --%>
		<%
			LocalDateTime dt = LocalDateTime.now();
			int heure = dt.getHour();
			if(heure>=7 && heure<18) {
		%>OUI<% } else { %>NON<% } %>
		</p>
	<% } %>
	<jsp:include page="pieddepage.inc.jsp">
		<jsp:param name="annee" value="2021" />
	</jsp:include>
</body>
</html>