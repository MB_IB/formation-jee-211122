<!DOCTYPE html><html>
<head>
	<meta charset="utf8">
	<title>Zoo</title>
</head>
<body>
	
	<%-- scriptlet --%>
	<% int surfaceTotale = NOMBRE_ENCLOS*14500; %>

	<!-- d�claration -->
	<%! final static int NOMBRE_ENCLOS = 13; %>

	<h1>Zoo !</h1>	
	<p>Notre zoo comporte <%-- expression --%><%= NOMBRE_ENCLOS %> enclos de 
		14500m� en moyenne, soit <%= surfaceTotale %> m� de surface animali�re.</p>
	<ul>
		<li><a href="ouverture">Horaires</a></li>
		<li><a href="enclos">Nos enclos</a></li>
		<li><a href="rapaces">Les rapaces</a></li>
		<li><a href="contact">Contact</a></li>
		<li><a href="visite">Ta visite au zoo</a></li>
		<li><a href="premiere_visite">1�re visite ?</a></li>
	</ul>
	
</body>
</html>