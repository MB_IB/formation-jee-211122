<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html><html>
<head><meta charset="utf8"><title>Zoo</title></head>
<body>
	<h1>Demande de devis</h1>
	<p>Merci de votre demande, elle est enregistrée.</p>
	<%-- 260e / personne --%>
	<p>Prix estimé : ${prix} € seulement</p>
	<p><a href="./">Retour à l'accueil</a></p>
</body>
</html>	
