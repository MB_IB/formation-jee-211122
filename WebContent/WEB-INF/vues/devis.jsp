<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html><html>
<head><meta charset="utf8"><title>Zoo</title></head>
<body>
	<h1>Demande de devis</h1>
	<p>Anniversaire ? Départ en retraite ? Organisez une visite 
	personnalisée VIP de notre zoo.</p>
	<form method="post">
		<p><label>Nom<input name="nom" value="${nom}"></label></p>
		<p><label>Email<input type="email" name="email" value="${email}"></label></p>
		<p><label>Nombre de visiteurs
			<input type="number" name="visiteurs" min="1" 
				max="${maxVisiteurs}" value="${visiteurs}">
		</label></p>
		<p><label>Date souhaitée<input type="date" name="date"></label></p>
		<p><label>Offre demandée<select name="offre">
			<c:forEach items="${offres}" var="offre">
				<option>${offre}</option>
			</c:forEach>
		</select></label></p>
		<p><input type="submit">${message}</p>
	</form>
</body>
</html>	