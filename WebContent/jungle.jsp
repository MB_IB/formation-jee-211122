<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="fr.ib.mickael.zoo.Animal" %>
<%@ page import="java.util.ArrayList" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<%
ArrayList<Animal> animaux = new ArrayList<>();
animaux.add(new Animal("Greg", "Leopard", 13));
animaux.add(new Animal("Hanna", "Python", 3));
animaux.add(new Animal("Isabelle", "Ouistiti", 5));
pageContext.setAttribute("animaux", animaux);
pageContext.setAttribute("maintenant", new java.util.Date());
%>
<!DOCTYPE html><html>
<head><meta charset="utf8"><title>Zoo</title></head>
<body>
	<h1>La jungle</h1>
	<c:if test="${animaux[0].age > 10}">
		<p>Le premier animal est vieux (aujourd'hui : 
			<fmt:formatDate value="${maintenant}" dateStyle="MEDIUM" /> ).
		</p>
	</c:if>
	<%-- Table des animaux, jeunes animaux d'abord : --%>
	<table>
		<thead><tr><th>Nom</th><th>Espèce</th><th>Age</th></tr></thead>
		<tbody>
		<c:forEach items="${animaux}" var="a">
			<c:if test="${a.age<10}">
				<tr>
					<td>${a.nom}</td>
					<td>${a.espece}</td>
					<td>
						<c:forEach var="n" begin="1" 
							end="${a.age}">*</c:forEach>
						<fmt:formatNumber value="${a.age}" 
							minIntegerDigits="2" /> 
					</td>
				</tr>
			</c:if>
		</c:forEach>
		<c:forEach items="${animaux}" var="a">
			<c:if test="${a.age>=10}">
				<tr>
					<td>${a.nom}</td>
					<td>${a.espece}</td>
					<td>
						<c:forEach var="n" begin="1" 
							end="${a.age}">*</c:forEach>
						${a.age}
					</td>
				</tr>
			</c:if>	
		</c:forEach>		
		</tbody>
	</table>	
</body>
</html>